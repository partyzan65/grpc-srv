module gitlab.com/partyzan65/grpc-srv

go 1.15

require (
	github.com/fatih/color v1.10.0
	github.com/gogo/protobuf v1.3.2
	github.com/golang/mock v1.4.4
	github.com/golang/protobuf v1.4.3
	github.com/grpc-ecosystem/go-grpc-middleware v1.2.2
	github.com/labstack/echo/v4 v4.1.17
	github.com/partyzanex/testutils v0.1.0
	github.com/pkg/errors v0.9.1
	github.com/rodaine/table v1.0.1
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/testify v1.7.0
	github.com/urfave/cli/v2 v2.3.0
	go.mongodb.org/mongo-driver v1.4.5
	google.golang.org/grpc v1.35.0
)
