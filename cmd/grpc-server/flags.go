package main

import "github.com/urfave/cli/v2"

var flags = []cli.Flag{ //nolint:gochecknoglobals
	&cli.StringFlag{
		Name:       "host",
		EnvVars:    []string{"GRPC_HOST"},
		Value:      "0.0.0.0",
		Required:   true,
		HasBeenSet: true,
	},
	&cli.UintFlag{
		Name:       "port",
		EnvVars:    []string{"GRPC_PORT"},
		Value:      5000, //nolint:gomnd
		Required:   true,
		HasBeenSet: true,
	},
	&cli.StringFlag{
		Name:       "mongo-url",
		EnvVars:    []string{"MONGODB_URL", "MONGO_URL"},
		Required:   true,
		HasBeenSet: true,
		Usage:      "ex. mongodb://localhost:27017/",
	},
	&cli.StringFlag{
		Name:       "mongo-database",
		EnvVars:    []string{"MONGODB_DATABASE", "MONGO_DATABASE"},
		Value:      "products",
		Required:   true,
		HasBeenSet: true,
	},
	&cli.BoolFlag{
		Name:    "debug",
		EnvVars: []string{"DEBUG"},
	},
	&cli.StringFlag{
		Name:    "log-format",
		EnvVars: []string{"LOG_FORMAT"},
		Aliases: []string{"f"},
	},
	&cli.BoolFlag{
		Name: "tls-enable",
	},
	&cli.StringFlag{
		Name:  "tls-cert-file",
		Value: "./configs/certs/server.crt",
	},
	&cli.StringFlag{
		Name:  "tls-key-file",
		Value: "./configs/certs/server.key",
	},
}
