package main

import (
	"crypto/tls"
	"fmt"
	"net"
	"os"
	"os/signal"

	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_recovery "github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/status"

	"gitlab.com/partyzan65/grpc-srv/internal/products"
	"gitlab.com/partyzan65/grpc-srv/pkg/fetcher/basehttp"
	"gitlab.com/partyzan65/grpc-srv/pkg/model"
	"gitlab.com/partyzan65/grpc-srv/pkg/repository/mongodb"
	"gitlab.com/partyzan65/grpc-srv/pkg/usecase/product"
)

func main() {
	app := cli.App{
		Usage:  "Products GRPC Server",
		Flags:  flags,
		Before: before,
		Action: action,
	}

	if err := app.Run(os.Args); err != nil {
		logrus.Fatal(err)
	}
}

func before(ctx *cli.Context) error {
	if ctx.Bool("debug") {
		logrus.SetLevel(logrus.DebugLevel)
	}

	if logFormat := ctx.String("log-format"); logFormat != "" {
		switch logFormat {
		case "json":
			logrus.SetFormatter(&logrus.JSONFormatter{})
		case "text":
			logrus.SetFormatter(&logrus.TextFormatter{})
		default:
			return errors.Errorf("unknown '%s' log format", logFormat)
		}
	}

	return nil
}

//nolint:funlen
func action(ctx *cli.Context) error {
	addr := fmt.Sprintf("%s:%d", ctx.String("host"), ctx.Uint("port"))

	lis, err := net.Listen("tcp", addr)
	if err != nil {
		return errors.Wrapf(err, "cannot listen address %s", addr)
	}

	client, err := mongo.Connect(ctx.Context, options.Client().ApplyURI(ctx.String("mongo-url")))
	if err != nil {
		return errors.Wrap(err, "cannot connect to mongodb")
	}

	db := client.Database(ctx.String("mongo-database"))

	// graceful shutdown
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)

	go func() {
		<-quit

		err := client.Disconnect(ctx.Context)
		if err != nil {
			logrus.Fatal(err)
		}

		err = lis.Close()
		if err != nil {
			logrus.Fatal(err)
		}
	}()

	useCase := product.UseCase{
		BatchSize:  2000, //nolint:gomnd
		Repository: mongodb.Products(db),
		Fetcher:    basehttp.New(nil),
	}
	svc := products.New(&useCase)

	customFunc := func(p interface{}) (err error) {
		return status.Errorf(codes.Unknown, "panic triggered: %v", p)
	}
	// Shared options for the logger, with a custom gRPC code to log level function.
	opts := []grpc_recovery.Option{
		grpc_recovery.WithRecoveryHandler(customFunc),
	}
	serverOpts := []grpc.ServerOption{
		grpc_middleware.WithUnaryServerChain(
			grpc_recovery.UnaryServerInterceptor(opts...),
		),
		grpc_middleware.WithStreamServerChain(
			grpc_recovery.StreamServerInterceptor(opts...),
		),
	}

	if ctx.Bool("tls-enable") {
		cert, err := tls.LoadX509KeyPair(ctx.String("tls-cert-file"), ctx.String("tls-key-file"))
		if err != nil {
			return errors.Wrap(err, "cannot load tls certificate")
		}

		serverOpts = append(serverOpts, grpc.Creds(credentials.NewServerTLSFromCert(&cert)))
	}

	srv := grpc.NewServer(serverOpts...)

	model.RegisterProductsServer(srv, svc)
	logrus.Debugf("Running GRPC Server on %s", addr)

	return srv.Serve(lis)
}
