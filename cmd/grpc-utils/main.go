package main

import (
	"os"

	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
)

func main() {
	app := cli.App{
		Name:  "grpc-utils",
		Usage: "GRPC Utils",
		Commands: cli.Commands{
			&migrate,
			&cliClient,
		},
	}

	if err := app.Run(os.Args); err != nil {
		logrus.Fatal(err)
	}
}
