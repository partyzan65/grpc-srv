package main

import (
	"context"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/fatih/color"
	"github.com/pkg/errors"
	"github.com/rodaine/table"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	"gitlab.com/partyzan65/grpc-srv/pkg/model"
	"gitlab.com/partyzan65/grpc-srv/pkg/utils"
)

var grpcClientKey = utils.NewCtxKey("grpc-client")

var cliClient = cli.Command{
	Name:  "client",
	Usage: "CLI Testing Tool for GRPC server",
	Flags: []cli.Flag{
		&cli.StringFlag{
			Name:       "host",
			EnvVars:    []string{"GRPC_HOST"},
			Value:      "0.0.0.0",
			Required:   true,
			HasBeenSet: true,
		},
		&cli.UintFlag{
			Name:       "port",
			EnvVars:    []string{"GRPC_PORT"},
			Value:      5000,
			Required:   true,
			HasBeenSet: true,
		},
		&cli.BoolFlag{
			Name: "tls-enable",
		},
		&cli.StringFlag{
			Name:  "tls-cert-file",
			Value: "./configs/certs/server.crt",
		},
		&cli.StringFlag{
			Name:  "tls-server-name",
			Value: "server",
		},
	},
	Before: func(ctx *cli.Context) error {
		addr := fmt.Sprintf("%s:%d", ctx.String("host"), ctx.Uint("port"))

		var opts []grpc.DialOption

		if ctx.Bool("tls-enable") {
			creds, err := credentials.NewClientTLSFromFile(ctx.String("tls-cert-file"), ctx.String("tls-server-name"))
			if err != nil {
				return errors.Wrap(err, "cannot load tls certificate")
			}

			opts = append(opts, grpc.WithTransportCredentials(creds))
		} else {
			opts = append(opts, grpc.WithInsecure())
		}

		conn, err := grpc.Dial(addr, opts...)
		if err != nil {
			return errors.Wrap(err, "cannot create dial connection")
		}

		client := model.NewProductsClient(conn)
		ctx.Context = context.WithValue(ctx.Context, grpcClientKey, client)

		return nil
	},
	Subcommands: cli.Commands{
		&cliClientFetch,
		&cliClientList,
	},
}

var cliClientFetch = cli.Command{
	Name:  "fetch",
	Usage: "Send a fetch request to GRPC server and output result",
	Flags: []cli.Flag{
		&cli.StringFlag{
			Name:     "url",
			Required: true,
		},
		&cli.BoolFlag{
			Name:  "has-header",
			Value: false,
		},
	},
	Action: func(ctx *cli.Context) error {
		client, ok := ctx.Context.Value(grpcClientKey).(model.ProductsClient)
		if !ok {
			return errors.New("no products client")
		}

		result, err := client.Fetch(ctx.Context, &model.FetchParams{
			URL:       ctx.String("url"),
			HasHeader: ctx.Bool("has-header"),
		})
		if err != nil {
			return errors.Wrap(err, "cannot fetch url")
		}

		if !result.Success {
			return errors.New("fetch failed!")
		}

		logrus.Info("Success!")

		return nil
	},
}

var cliClientList = cli.Command{ //nolint:gochecknoglobals
	Name:  "list",
	Usage: "Get a list of products from GRPC server and output result in stdout",
	Flags: []cli.Flag{
		&cli.IntFlag{
			Name: "limit",
		},
		&cli.IntFlag{
			Name: "offset",
		},
		&cli.StringFlag{
			Name: "sort",
			Usage: "sort column name, available name, price, price_count, " +
				"for descending sort should use -name, -price, -price_count",
		},
		&cli.StringFlag{
			Name:    "format",
			Aliases: []string{"f"},
			Usage:   "Output format, available json, csv, cli",
			Value:   "cli",
		},
		&cli.BoolFlag{
			Name:  "with-headers",
			Value: true,
		},
	},
	Action: func(ctx *cli.Context) error {
		client, ok := ctx.Context.Value(grpcClientKey).(model.ProductsClient)
		if !ok {
			return errors.New("no products client")
		}

		request := model.ProductsRequest{
			Limit:  int32(ctx.Int("limit")),
			Offset: int32(ctx.Int("offset")),
		}

		if sort := ctx.String("sort"); sort != "" {
			sortParams := model.SortParams{}

			if strings.HasPrefix(sort, "-") {
				sortParams.Direction = model.Direction_DESC
				sort = strings.Replace(sort, "-", "", 1)
			}

			switch sort {
			case "name":
				sortParams.Column = model.Column_NAME
			case "price":
				sortParams.Column = model.Column_PRICE
			case "price_count":
				sortParams.Column = model.Column_PRICE_COUNT
			default:
				return errors.Errorf("unknown sort column %s", sort)
			}

			request.Sort = &sortParams
		}

		response, err := client.List(ctx.Context, &request)
		if err != nil {
			return errors.Wrap(err, "cannot get list of products")
		}

		switch ctx.String("format") {
		case "json":
			return json.NewEncoder(os.Stdout).Encode(response)
		case "csv":
			return encodeToCSV(response, ctx.Bool("with-headers"))
		case "cli":
			return encodeToCli(response)
		default:
			return errors.Errorf("unknown output format %s", ctx.String("format"))
		}
	},
}

func encodeToCSV(response *model.ProductsResponse, headers bool) error {
	w := csv.NewWriter(os.Stdout)
	w.UseCRLF = true

	if headers {
		err := w.Write([]string{
			"UID",
			"Product Name",
			"Price",
			"Price Count",
			"Price Changed",
		})
		if err != nil {
			return errors.Wrap(err, "cannot write csv headers")
		}
	}

	for _, product := range response.Results {
		err := w.Write([]string{
			product.GetUID(),
			product.Name,
			product.Price.String(),
			strconv.Itoa(int(product.PriceCount)),
			time.Unix(product.DTChangePrice, 0).String(),
		})
		if err != nil {
			return errors.Wrapf(err, "cannot write product %v", *product)
		}
	}

	w.Flush()

	return nil
}

func encodeToCli(response *model.ProductsResponse) error {
	headerFmt := color.New(color.FgGreen, color.Underline).SprintfFunc()
	columnFmt := color.New(color.FgYellow).SprintfFunc()
	tbl := table.New("UID", "Product Name", "Price", "Price Count", "Price Changed")
	tbl.WithHeaderFormatter(headerFmt).WithFirstColumnFormatter(columnFmt)

	for _, product := range response.Results {
		tbl.AddRow(
			product.GetUID(),
			product.Name,
			product.Price.String(),
			strconv.Itoa(int(product.PriceCount)),
			time.Unix(product.DTChangePrice, 0).String(),
		)
	}

	tbl.Print()

	return nil
}
