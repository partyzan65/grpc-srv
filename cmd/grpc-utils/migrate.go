package main

import (
	"context"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"gitlab.com/partyzan65/grpc-srv/pkg/utils"
)

var mongoClientKey = utils.NewCtxKey("mongo-client")

var migrate = cli.Command{
	Name:  "migrate",
	Usage: "Database Migration Tool",
	Flags: []cli.Flag{
		&cli.StringFlag{
			Name:       "mongo-url",
			EnvVars:    []string{"MONGODB_URL", "MONGO_URL"},
			Required:   true,
			HasBeenSet: true,
			Usage:      "ex. mongodb://localhost:27017/",
		},
		&cli.StringFlag{
			Name:       "mongo-database",
			EnvVars:    []string{"MONGODB_DATABASE", "MONGO_DATABASE"},
			Value:      "products",
			Required:   true,
			HasBeenSet: true,
		},
	},
	Before: func(ctx *cli.Context) error {
		client, err := mongo.Connect(ctx.Context, options.Client().ApplyURI(ctx.String("mongo-url")))
		if err != nil {
			return errors.Wrap(err, "cannot connect to mongodb")
		}

		ctx.Context = context.WithValue(ctx.Context, mongoClientKey, client)

		return nil
	},
	Subcommands: cli.Commands{
		&migrateUp,
		&migrateCleanup,
	},
	After: func(ctx *cli.Context) error {
		client, ok := ctx.Context.Value(mongoClientKey).(*mongo.Client)
		if !ok {
			return errors.New("no mongo client")
		}

		return client.Disconnect(ctx.Context)
	},
}

var migrateUp = cli.Command{
	Name: "up",
	Action: func(ctx *cli.Context) error {
		client, ok := ctx.Context.Value(mongoClientKey).(*mongo.Client)
		if !ok {
			return errors.New("no mongo client")
		}

		db := client.Database(ctx.String("mongo-database"))

		err := db.CreateCollection(ctx.Context, "products")
		if err != nil {
			return errors.Wrap(err, "cannot create products collection")
		}

		col := db.Collection("products")

		_, err = col.Indexes().CreateMany(ctx.Context, []mongo.IndexModel{
			{
				Keys: bson.M{
					"uid": 1,
				},
			},
			{
				Keys: bson.M{
					"name": 1,
				},
			},
			{
				Keys: bson.M{
					"price": 1,
				},
			},
		})
		if err != nil {
			return errors.Wrap(err, "cannot create indexes")
		}

		logrus.Info("Migrations applied successfully!")

		return nil
	},
}

var migrateCleanup = cli.Command{ //nolint:gochecknoglobals
	Name: "cleanup",
	Action: func(ctx *cli.Context) error {
		client, ok := ctx.Context.Value(mongoClientKey).(*mongo.Client)
		if !ok {
			return errors.New("no mongo client")
		}

		db := client.Database(ctx.String("mongo-database"))
		col := db.Collection("products")

		err := col.Drop(ctx.Context)
		if err != nil {
			return errors.Wrap(err, "cannot drop collection")
		}

		logrus.Info("Cleanup database successfully!")

		return nil
	},
}
