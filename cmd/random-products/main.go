package main

import (
	"os"

	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/partyzan65/grpc-srv/internal/random"
)

func main() {
	app := cli.App{
		Name: "random-products",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:       "host",
				Value:      "0.0.0.0",
				Required:   true,
				HasBeenSet: true,
				EnvVars:    []string{"RANDOM_PRODUCTS_HOST"},
			},
			&cli.UintFlag{
				Name:       "port",
				Value:      5050, //nolint:gomnd
				Required:   true,
				HasBeenSet: true,
				EnvVars:    []string{"RANDOM_PRODUCTS_PORT"},
			},
		},
		Action: func(ctx *cli.Context) error {
			server := random.Server{
				Host: ctx.String("host"),
				Port: uint16(ctx.Uint("port")),
			}

			return server.Run()
		},
	}

	if err := app.Run(os.Args); err != nil {
		logrus.Fatal(err)
	}
}
