package random

import (
	"encoding/csv"
	"fmt"
	"math/rand"
	"net/http"
	"strconv"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

type Server struct {
	Host string
	Port uint16
}

func (s *Server) Run() error {
	e := echo.New()
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.GET("/products.csv", func(ctx echo.Context) error {
		var (
			limit = defaultLimit
			err   error
		)

		if lim := ctx.QueryParam("limit"); lim != "" {
			limit, err = strconv.Atoi(lim)
			if err != nil {
				return echo.NewHTTPError(http.StatusBadRequest, err.Error())
			}
		}

		ctx.Response().Header().Set(echo.HeaderContentType, "text/csv")
		ctx.Response().Header().Set(echo.HeaderContentDisposition, "attachment; filename=products.csv")

		w := csv.NewWriter(ctx.Response())
		w.UseCRLF = true

		err = w.Write([]string{"Product Name", "Price"})
		if err != nil {
			return echo.NewHTTPError(http.StatusInternalServerError, "cannot write csv header").
				SetInternal(err)
		}

		rnd := rand.New(rand.NewSource(time.Now().UnixNano()))

		for i := 0; i < limit; i++ {
			p := createProduct(rnd)

			err := w.Write([]string{p.Name, p.Price.String()})
			if err != nil {
				return echo.NewHTTPError(http.StatusInternalServerError, "cannot write product").
					SetInternal(err)
			}
		}

		w.Flush()

		return nil
	})

	return e.Start(fmt.Sprintf("%s:%d", s.Host, s.Port))
}
