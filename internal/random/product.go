package random

import (
	"math/rand"
	"strings"

	"github.com/partyzanex/testutils"

	"gitlab.com/partyzan65/grpc-srv/pkg/model"
)

type product struct {
	Name  string
	Price model.Price
}

var (
	productKeys1 = []string{ //nolint:gochecknoglobals
		"Smartphone", "Notebook", "Monitor", "HDD", "Pen", "Paper", "Smart Watch",
		"Washing Machine", "Meat Grinder", "Hoover", "Kettle", "Water Boiler", "Hair Dryer", "Kitchen Stove",
	}
	productKeys2 = []string{ //nolint:gochecknoglobals
		"Samsung", "Apple", "Huawei", "Xiaomi", "Honor", "Sony", "LG",
		"Lenovo", "Nokia", "Fly", "ZTE", "vivo", "Wigor", "Realme",
	}
	productKeys3 = []string{ //nolint:gochecknoglobals
		"Red", "Blue", "Green", "White", "Black", "Grey", "Brown", "Yellow",
		"Magenta", "Cyan", "Dark Blue", "Aqua", "Pink", "Orange", "Silver", "Teal",
	}
	n1 = len(productKeys1) //nolint:gochecknoglobals
	n2 = len(productKeys1) //nolint:gochecknoglobals
	n3 = len(productKeys1) //nolint:gochecknoglobals
)

const (
	space        = " "
	defaultLimit = 100
)

func createProduct(rnd *rand.Rand) product {
	name := strings.Builder{}
	name.WriteString(productKeys1[rnd.Intn(n1)])
	name.WriteString(space)
	name.WriteString(productKeys2[rnd.Intn(n2)])
	name.WriteString(space)
	name.WriteString(productKeys3[rnd.Intn(n3)])
	name.WriteString(space)
	name.WriteString(strings.ToUpper(testutils.RandomString(2)))

	return product{
		Name: name.String(),
		Price: model.Price{
			Amount:   testutils.RandInt64(1000, 100000),
			Currency: "USD",
		},
	}
}
