package products

import (
	"context"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"

	"gitlab.com/partyzan65/grpc-srv/pkg/model"
	"gitlab.com/partyzan65/grpc-srv/pkg/usecase/product"
)

// products implements of model.ProductsServer interface.
type products struct {
	// embed of model.UnimplementedProductsServer
	model.UnimplementedProductsServer
	// products pointer of *product.UseCase
	products *product.UseCase
}

// New creates a copy of model.ProductsServer interface.
func New(useCase *product.UseCase) model.ProductsServer {
	return &products{
		products: useCase,
	}
}

// List implements of model.ProductsServer.
func (s *products) List(ctx context.Context, request *model.ProductsRequest) (*model.ProductsResponse, error) {
	results, count, err := s.products.List(ctx, request)
	if err != nil {
		logrus.Debugf("cannot get list of products: %s", err)
		return nil, errors.Wrap(err, "cannot get list of products")
	}

	// debug info
	logrus.WithContext(ctx).
		WithField("request", request).
		WithField("count", count).
		Debug("List")

	return &model.ProductsResponse{
		Results: results,
		Count:   int32(count),
		Limit:   request.Limit,
		Offset:  request.Offset,
	}, nil
}

// Fetch implements of model.ProductsServer.
func (s *products) Fetch(ctx context.Context, params *model.FetchParams) (*model.FetchResult, error) {
	err := s.products.Fetch(ctx, params)
	if err != nil {
		logrus.Debugf("cannot fetch products: %s", err)
		return nil, errors.Wrap(err, "cannot fetch products")
	}

	// debug info
	logrus.WithContext(ctx).
		WithField("params", params).
		Debug("Fetch")

	return &model.FetchResult{
		Success: true,
	}, nil
}

// Healthz implements of model.ProductsServer.
func (*products) Healthz(ctx context.Context, _ *model.Health) (*model.Health, error) {
	return &model.Health{
		Success: true,
	}, nil
}
