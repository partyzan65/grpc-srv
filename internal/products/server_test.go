package products_test

import (
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"strings"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/partyzanex/testutils"
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc"

	"gitlab.com/partyzan65/grpc-srv/internal/products"
	"gitlab.com/partyzan65/grpc-srv/pkg/model"
	"gitlab.com/partyzan65/grpc-srv/pkg/usecase/product"

	fetcher "gitlab.com/partyzan65/grpc-srv/pkg/fetcher/mockups"
	mongo "gitlab.com/partyzan65/grpc-srv/pkg/repository/mockups"
)

const csvRows = `Product Name,Price
Kitchen Stove Huawei Brown FMN,252.07 USD
Water Boiler ZTE White 1CK,698.76 USD
Hoover Nokia Grey ZOY,824.87 USD
`

func TestProducts_Fetch(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	testURL := testutils.RandomString(50)
	exp := &model.FetchResult{
		Success: true,
	}
	expResponse := &model.ProductsResponse{
		Results: []*model.Product{
			{
				UID:  testutils.RandomString(32),
				Name: testutils.RandomString(64),
				Price: model.Price{
					Amount:   testutils.RandInt64(1000, 100000),
					Currency: testutils.RandomCase("USD", "EUR", "RUB").(string),
				},
				PriceCount:    int32(testutils.RandInt(0, 120)),
				DTChangePrice: time.Now().Unix(),
				PriceLogs:     nil,
			},
			{
				UID:  testutils.RandomString(32),
				Name: testutils.RandomString(64),
				Price: model.Price{
					Amount:   testutils.RandInt64(1000, 100000),
					Currency: testutils.RandomCase("USD", "EUR", "RUB").(string),
				},
				PriceCount:    int32(testutils.RandInt(0, 120)),
				DTChangePrice: time.Now().Unix(),
				PriceLogs:     nil,
			},
		},
		Count:  2,
		Limit:  1000,
		Offset: 1000,
	}

	rc := ioutil.NopCloser(strings.NewReader(csvRows))

	fetcherMock := fetcher.NewMockInterface(ctrl)
	fetcherMock.EXPECT().Fetch(gomock.Any(), testURL).Return(rc, nil)
	request := model.ProductsRequest{
		Limit:  1000,
		Offset: 1000,
		Sort: &model.SortParams{
			Column:    model.Column_PRICE,
			Direction: model.Direction_DESC,
		},
	}
	repoMock := mongo.NewMockProduct(ctrl)
	repoMock.EXPECT().Save(gomock.Any(), gomock.Any()).Return(nil).AnyTimes()
	repoMock.EXPECT().Find(gomock.Any(), &request).Return(expResponse.Results, 2, nil)

	useCase := product.UseCase{
		Repository: repoMock,
		Fetcher:    fetcherMock,
	}

	addr := fmt.Sprintf("localhost:%d", testutils.RandInt(30000, 40000))
	svc := products.New(&useCase)
	srv := grpc.NewServer()
	model.RegisterProductsServer(srv, svc)

	lis, err := net.Listen("tcp", addr)
	assert.Equal(t, nil, err)

	done := make(chan struct{})

	go func() {
		<-done

		if err := lis.Close(); err != nil {
			log.Println("close error:", err)
		}
	}()

	go func() {
		_ = srv.Serve(lis)
	}()

	t.Cleanup(func() {
		done <- struct{}{}
	})

	conn, err := grpc.Dial(addr, grpc.WithInsecure())
	assert.Equal(t, nil, err)

	client := model.NewProductsClient(conn)
	ctx := context.Background()

	for {
		_, err := client.Healthz(ctx, &model.Health{})
		if err == nil {
			break
		}
	}

	got, err := client.Fetch(ctx, &model.FetchParams{
		URL:       testURL,
		HasHeader: true,
	})
	if !assert.Equal(t, nil, err) {
		t.Log(err)
	}

	assert.Equal(t, exp, got)

	response, err := client.List(ctx, &request)
	if !assert.Equal(t, nil, err) {
		t.Log(err)
	}

	assert.Equal(t, expResponse, response)
}
