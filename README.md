# grpc-srv

## Структура проекта
```bash
.
├── build                   # директория для билдов
├── cmd                     # для CLI
    ├── grpc-server         # CLI запуска GRPC-сервиса
    ├── grpc-utils          # CLI для миграций и GRPC-клиента
    ├── random-products     # CLI запуска HTTP-сервера
├── configs                 # для конфигов и сертификатов (TLS)
    ├── certs
├── internal                
    ├── products            # реализация GRPC-сервиса
    ├── random              # HTTP-сервер, отдает рандомные продукты
├── pkg
    ├── fetcher
    ├── model               # директория для моделей, в т.ч. для *.proto
    ├── repository          # директория для репозиториев
    ├── usecase             # юзекейсы
    ├── utils               # вспомогательные функции
```

## Основные фичи
- Clean Architecture
- Gitlab CI
- HAProxy в качестве балансировщика
- MongoDB (не кластер)
- Protobuf

## Запуск в Docker

```bash
make $GOPATH/src/gitlab.com/partyzan65
cd $GOPATH/src/gitlab.com/partyzan65
git clone https://gitlab.com/partyzan65/grpc-srv.git
cd grpc-srv
docker-compose up
```

## Проверка

```bash
cd $GOPATH/src/gitlab.com/partyzan65/grpc-srv
// билдим CLI утилиту
make build-grpc-utils
// дергаем за fetch
./build/grpc-utils client fetch --url="http://domain.com/products.csv" --has-header
```

Если нет под рукой сервера с `products.csv` в правильном формате 
`PRODUCT NAME,PRICE` - это не проблема, я сделал сервис, который 
крутится на 5050-м порту и отдает список с рандомными товарами:

```bash
curl --location --request GET 'http://localhost:5050/products.csv?limit=1000'
```
`limit` - это количество товаров, без ограничений, по умолчанию - `100`

Внутри контейнера этот сервис доступен по адресу `http://random-products:5050/products.csv`
```bash
./build/grpc-utils client fetch --url="http://random-products:5050/products.csv" --has-header
# загрузим больше товаров
./build/grpc-utils client fetch --url="http://random-products:5050/products.csv?limit=10000" --has-header
# здесь уже придется немного подождать
./build/grpc-utils client fetch --url="http://random-products:5050/products.csv?limit=100000" --has-header
```

Вывод результатов:
```bash
./build/grpc-utils client list

# постранично
./build/grpc-utils client list --limit=100 --offset=100

# сортировка
./build/grpc-utils client list --sort=-price_count --limit=10

# вывод в json, кстати с логом изменений цен для каждого товара
./build/grpc-utils client list --sort=-price_count --limit=10 -f json

# вывод в csv
./build/grpc-utils client list --sort=-price_count --limit=10 -f csv
```

