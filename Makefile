.PHONY: proto
proto:
	protoc -I=./pkg/model -I=${GOPATH}/src \
	-I=${GOPATH}/src/github.com/gogo/protobuf/protobuf \
	--gofast_out=plugins=grpc:./pkg/model ./pkg/model/*.proto

.PHONY: mockups
mockups:
	export PWD=$(pwd)
	cd ${PWD}/pkg/fetcher/ && go generate
	cd ${PWD}/pkg/repository/ && go generate

.PHONY: test
test:
	CGO_ENABLED=0 go test -v ./internal/products -count=1
	CGO_ENABLED=0 go test -v ./pkg/repository/mongodb -count=1

.PHONY: build-grpc-server
build-grpc-server:
	CGO_ENABLED=0 go build -o ./build/grpc-server ./cmd/grpc-server/

.PHONY: build-grpc-utils
build-grpc-utils:
	CGO_ENABLED=0 go build -o ./build/grpc-utils ./cmd/grpc-utils/

.PHONY: build-random-products
build-random-products:
	CGO_ENABLED=0 go build -o ./build/random-products ./cmd/random-products/

.PHONY: build
build: build-grpc-server build-grpc-utils build-random-products

.PHONY: certstrap-local
certstrap-local:
	cd /tmp && git clone https://github.com/square/certstrap && \
	cd /tmp/certstrap && git checkout v1.2.0 && \
	go build && mv certstrap ${GOPATH}/bin/certstrap && \
	chmod +x ${GOPATH}/bin/certstrap

.PHONY: certs
certs:
	cd ./configs/certs && certstrap init --common-name=server --passphrase="" && \
	mv ./out/* ./ && rm -rf ./out
