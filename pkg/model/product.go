package model

import (
	"crypto/md5" //nolint:gosec
	"encoding/hex"
	"encoding/json"
	"fmt"
	"strconv"
	"strings"

	"github.com/pkg/errors"

	"gitlab.com/partyzan65/grpc-srv/pkg/utils"
)

// GetUID set UID if empty and returns it.
func (m *Product) GetUID() string {
	if m.UID == "" {
		m.UID = UID(m.Name)
	}

	return m.UID
}

// MarshalJSON reimplements of json.Marshaler interface.
func (m Price) MarshalJSON() ([]byte, error) {
	return json.Marshal(m.String())
}

// String reimplements of fmt.Stringer interface.
func (m Price) String() string {
	return fmt.Sprintf("%.2f %s", float64(m.Amount)/100, m.Currency)
}

// ParsePrice parses Price from formatted string.
func ParsePrice(str string) (*Price, error) {
	str = strings.Trim(str, " ")
	p := strings.Split(str, " ")

	if len(p) != 2 {
		return nil, errors.New("wrong price value")
	}

	v, err := strconv.ParseFloat(p[0], 64)
	if err != nil {
		return nil, errors.Wrap(err, "cannot parse price value")
	}

	return &Price{
		Amount:   int64(utils.FixedFloat(v, 2) * 100),
		Currency: p[1],
	}, nil
}

// UID create a md5 hash from string.
func UID(str string) string {
	h := md5.Sum([]byte(str)) //nolint:gosec

	return hex.EncodeToString(h[:])
}
