package mongodb

import (
	"context"
	"sync"
	"time"

	"github.com/pkg/errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"gitlab.com/partyzan65/grpc-srv/pkg/model"
	"gitlab.com/partyzan65/grpc-srv/pkg/repository"
	"gitlab.com/partyzan65/grpc-srv/pkg/utils"
)

const (
	defaultLimit = int64(100)
	maxLimit     = int64(1000)
)

// products implements of repository.Product interface.
type products struct {
	db *mongo.Database
}

// Products creates a copy of repository.Product interface.
func Products(db *mongo.Database) repository.Product {
	return &products{
		db: db,
	}
}

// Find search products and returns []*model.Product slice and count.
func (repo *products) Find(ctx context.Context, request *model.ProductsRequest) ([]*model.Product, int, error) {
	productsCol := repo.db.Collection("products")
	opts := options.Find().SetLimit(defaultLimit)

	// get request params
	if request != nil && request.Limit > 0 {
		limit := int64(request.Limit)

		if limit > maxLimit {
			limit = maxLimit
		}

		opts.SetLimit(limit)

		if request.Offset > 0 {
			opts.SetSkip(int64(request.Offset))
		}

		if request.Sort != nil && request.Sort.Column > 0 {
			dir := 1

			if request.Sort.Direction > 0 {
				dir = -1
			}

			switch request.Sort.Column {
			case model.Column_NAME:
				opts.SetSort(bson.D{{"name", dir}})
			case model.Column_PRICE:
				opts.SetSort(bson.D{{"price", dir}})
			case model.Column_PRICE_COUNT:
				opts.SetSort(bson.D{{"price_count", dir}})
			default:
				return nil, 0, errors.New("invalid sort params")
			}
		}
	}

	var (
		results []*model.Product
		count   int64
		errs    []error
		wg      = sync.WaitGroup{}
		mx      = &sync.Mutex{}
	)

	wg.Add(2)

	go func() {
		defer wg.Done()
		var err error

		count, err = productsCol.CountDocuments(ctx, bson.D{})
		if err != nil {
			utils.AppendErr(mx, &errs, errors.Wrap(err, "cannot get count of products"))
		}
	}()

	go func() {
		defer wg.Done()

		cursor, err := productsCol.Find(ctx, bson.D{}, opts)
		if err != nil {
			utils.AppendErr(mx, &errs, errors.Wrap(err, "cannot find products"))
			return
		}

		err = cursor.All(ctx, &results)
		if err != nil {
			utils.AppendErr(mx, &errs, errors.Wrap(err, "cannot get results"))
		}
	}()

	wg.Wait()

	return results, int(count), utils.JoinErrs(errs...)
}

func (repo *products) Save(ctx context.Context, products ...*model.Product) error {
	uids := make([]string, len(products))
	productByUID := make(map[string]*model.Product)
	now := time.Now().Unix()

	for i, product := range products {
		uids[i] = product.GetUID()
		productByUID[product.UID] = product
	}

	productsCol := repo.db.Collection("products")

	cursor, err := productsCol.Find(ctx, bson.M{"uid": bson.D{{"$in", uids}}})
	if err != nil {
		return errors.Wrap(err, "cannot find products")
	}

	for cursor.Next(ctx) {
		pr := model.Product{}

		err := cursor.Decode(&pr)
		if err != nil {
			return errors.Wrap(err, "cannot decode product")
		}

		found, ok := productByUID[pr.UID]
		if !ok {
			return errors.Wrap(err, "cannot find product for update")
		}

		if pr.Price != found.Price {
			if found.DTChangePrice <= 0 {
				found.DTChangePrice = now
			}

			found.PriceLogs = pr.PriceLogs

			if found.PriceLogs == nil {
				found.PriceLogs = make([]*model.PriceLog, 0)
			}

			found.PriceLogs = append(found.PriceLogs, &model.PriceLog{
				Old: &pr.Price,
				New: &found.Price,
				DT:  found.DTChangePrice,
			})

			found.PriceCount = int32(len(found.PriceLogs))
		}

		_, err = productsCol.UpdateOne(ctx,
			bson.M{"uid": bson.M{"$eq": found.UID}},
			bson.M{"$set": found},
		)
		if err != nil {
			return errors.Wrap(err, "cannot update product")
		}

		delete(productByUID, found.UID)
	}

	toInsert := make([]interface{}, len(productByUID))
	i := 0

	for _, product := range productByUID {
		if product.DTChangePrice <= 0 {
			product.DTChangePrice = now
		}

		toInsert[i] = product
		i++
	}

	if i == 0 {
		return nil
	}

	_, err = productsCol.InsertMany(ctx, toInsert)
	if err != nil {
		return errors.Wrap(err, "cannot insert products")
	}

	return nil
}

func (repo *products) Delete(ctx context.Context, products ...*model.Product) error {
	uids := make([]string, len(products))

	for i, product := range products {
		uids[i] = product.GetUID()
	}

	productsCol := repo.db.Collection("products")

	_, err := productsCol.DeleteMany(ctx, bson.M{
		"uid": bson.D{{"$in", uids}},
	})
	if err != nil {
		return errors.Wrap(err, "cannot delete products")
	}

	return nil
}
