package mongodb_test

import (
	"context"
	"os"
	"testing"
	"time"

	"github.com/partyzanex/testutils"
	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"gitlab.com/partyzan65/grpc-srv/pkg/model"
	"gitlab.com/partyzan65/grpc-srv/pkg/repository/mongodb"
)

func TestProducts_Integrative(t *testing.T) {
	db := getTestDB(t)
	ctx := context.Background()

	t.Cleanup(func() {
		err := db.Client().Disconnect(ctx)
		assert.Equal(t, nil, err)
	})

	_ = db.CreateCollection(ctx, "products")
	repo := mongodb.Products(db)
	expected := getExpected()

	err := repo.Save(ctx, expected...)
	if !assert.Equal(t, nil, err) {
		t.Log(err)
	}

	expectedByUID := make(map[string]*model.Product)

	for _, p := range expected {
		p.Price.Amount = testutils.RandInt64(1000, 100000)
		expectedByUID[p.GetUID()] = p
	}

	err = repo.Save(ctx, expected...)
	if !assert.Equal(t, nil, err) {
		t.Log(err)
	}

	t.Cleanup(func() {
		err := repo.Delete(ctx, expected...)
		if !assert.Equal(t, nil, err) {
			t.Log(err)
		}
	})

	got, count, err := repo.Find(ctx, nil)
	if !assert.Equal(t, nil, err) {
		t.Log(err)
	}

	if assert.Equal(t, 2, count) {
		for _, p := range got {
			exp, ok := expectedByUID[p.GetUID()]
			if assert.Equal(t, true, ok) {
				assert.EqualValues(t, exp, p)
			}
		}
	}
}

func getTestDB(t *testing.T) *mongo.Database {
	mongoURL := os.Getenv("TEST_MONGODB_URL")
	if mongoURL == "" {
		t.Skip("empty TEST_MONGODB_URL")
	}

	mongoDB := os.Getenv("TEST_MONGODB_DATABASE")
	if mongoDB == "" {
		t.Skip("empty TEST_MONGODB_DATABASE")
	}

	ctx := context.Background()

	client, err := mongo.Connect(ctx, options.Client().ApplyURI(mongoURL))
	if !assert.Equal(t, nil, err) {
		t.Fatal(err)
	}

	return client.Database(mongoDB)
}

func getExpected() []*model.Product {
	return []*model.Product{
		{
			Name: "Test Product",
			Price: model.Price{
				Amount:   testutils.RandInt64(1000, 100000),
				Currency: "USD",
			},
			DTChangePrice: time.Now().UnixNano(),
		},
		{
			Name: testutils.RandomString(64),
			Price: model.Price{
				Amount:   testutils.RandInt64(1000, 100000),
				Currency: "USD",
			},
			DTChangePrice: time.Now().UnixNano(),
		},
	}
}
