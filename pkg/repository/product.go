package repository

import (
	"context"

	"gitlab.com/partyzan65/grpc-srv/pkg/model"
)

//go:generate mockgen -source=product.go -destination=./mockups/product.go -package=mockups

// Product represent a repository layer for products storage.
type Product interface {
	// Find search products and returns []*model.Product slice and count
	Find(ctx context.Context, request *model.ProductsRequest) ([]*model.Product, int, error)
	// Save updates or inserts products or returns error
	Save(ctx context.Context, products ...*model.Product) error
	// Delete deletes products from storage
	Delete(ctx context.Context, products ...*model.Product) error
}
