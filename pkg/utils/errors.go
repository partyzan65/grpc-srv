package utils

import (
	"errors"
	"strings"
	"sync"
)

func AppendErr(mu sync.Locker, errs *[]error, err error) {
	if mu != nil {
		mu.Lock()
		defer mu.Unlock()
	}

	*errs = append(*errs, err)
}

func JoinErrs(errs ...error) error {
	n := len(errs)
	if n == 0 {
		return nil
	}

	slice := make([]string, n)

	for i, err := range errs {
		slice[i] = err.Error()
	}

	return errors.New(strings.Join(slice, "; ")) // nolint:err113
}
