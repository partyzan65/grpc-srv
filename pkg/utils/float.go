package utils

import "math"

func FixedFloat(num, pow float64) float64 {
	output := math.Pow(10, pow) //nolint:gomnd

	return math.Round(num*output) / output
}
