package utils_test

import (
	"errors"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/partyzan65/grpc-srv/pkg/utils"
)

func TestAppendErr(t *testing.T) { //nolint:paralleltest
	var (
		wg   sync.WaitGroup
		mu   sync.Mutex
		errs []error
		n    = 10
	)

	for i := 0; i < n; i++ {
		wg.Add(1)

		go func() {
			utils.AppendErr(&mu, &errs, errors.New("test"))
			wg.Done()
		}()
	}

	wg.Wait()

	assert.Equal(t, n, len(errs))
}

func TestJoinErrs(t *testing.T) {
	t.Parallel()

	errs := []error{
		errors.New("test 1"),
		errors.New("test 2"),
		errors.New("test 3"),
		errors.New("test 4"),
	}

	err := utils.JoinErrs(errs...)
	assert.Equal(t, true, err != nil)

	if assert.Error(t, err) {
		assert.Equal(t, "test 1; test 2; test 3; test 4", err.Error())
	}
}
