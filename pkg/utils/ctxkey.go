package utils

type CtxKey struct {
	name string
}

func NewCtxKey(name string) CtxKey {
	return CtxKey{
		name: name,
	}
}
