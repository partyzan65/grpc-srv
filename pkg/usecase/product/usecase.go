package product

import (
	"context"
	"encoding/csv"
	"io"

	"github.com/pkg/errors"

	"gitlab.com/partyzan65/grpc-srv/pkg/fetcher"
	"gitlab.com/partyzan65/grpc-srv/pkg/model"
	"gitlab.com/partyzan65/grpc-srv/pkg/repository"
)

const (
	DefaultBatchSize = 100
)

// UseCase combines logic of repository.Product and fetcher.Interface.
type UseCase struct {
	// BatchSize is limit on the count of saved products at one time
	BatchSize int
	// Repository is copy of repository.Product interface
	Repository repository.Product
	// Fetcher is a copy of fetcher.Interface
	Fetcher fetcher.Interface
}

// Save ensures products in storage.
func (uc *UseCase) Save(ctx context.Context, products ...*model.Product) error {
	return uc.Repository.Save(ctx, products...)
}

// List search a products and returns slice []*model.Product and count of products.
func (uc *UseCase) List(ctx context.Context, request *model.ProductsRequest) ([]*model.Product, int, error) {
	results, count, err := uc.Repository.Find(ctx, request)
	if err != nil {
		return nil, 0, errors.Wrap(err, "cannot get list of products")
	}

	return results, count, nil
}

// Fetch downloads a document and parses it.
func (uc *UseCase) Fetch(ctx context.Context, params *model.FetchParams) error {
	if params == nil {
		return errors.New("required params")
	}

	batchSize := uc.BatchSize

	if batchSize == 0 {
		batchSize = DefaultBatchSize
	}

	reader, err := uc.Fetcher.Fetch(ctx, params.URL)
	if err != nil {
		return err
	}

	defer func() {
		_ = reader.Close()
	}()

	r := csv.NewReader(reader)
	i, c := 0, 0
	batch := make([]*model.Product, 0)

	for {
		i++

		row, err := r.Read()
		if err == io.EOF {
			break
		}

		if params.HasHeader && i == 1 {
			continue
		}

		if err != nil {
			return errors.Wrap(err, "cannot read line")
		}

		if !(len(row) >= 2) {
			return errors.New("wrong count of columns")
		}

		price, err := model.ParsePrice(row[1])
		if err != nil {
			return errors.Wrap(err, "cannot parse price")
		}

		batch = append(batch, &model.Product{
			UID:   model.UID(row[0]),
			Name:  row[0],
			Price: *price,
		})
		c++

		if c == batchSize {
			err = uc.Save(ctx, batch...)
			if err != nil {
				return errors.Wrap(err, "cannot save product")
			}

			c = 0
			batch = make([]*model.Product, 0)
		}
	}

	if c > 0 {
		err = uc.Save(ctx, batch...)
		if err != nil {
			return errors.Wrap(err, "cannot save product")
		}
	}

	return nil
}
