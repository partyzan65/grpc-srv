package basehttp

import (
	"context"
	"io"
	"net/http"

	"github.com/pkg/errors"

	"gitlab.com/partyzan65/grpc-srv/pkg/fetcher"
)

// httpFetcher implement a fetcher.Interface.
type httpFetcher struct {
	*http.Client
}

// New create a copy of fetcher.Interface.
func New(client *http.Client) fetcher.Interface {
	if client == nil {
		client = http.DefaultClient
	}

	return &httpFetcher{
		Client: client,
	}
}

// Fetch download document by URL and returns io.ReadCloser or error.
func (c *httpFetcher) Fetch(ctx context.Context, url string) (io.ReadCloser, error) {
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, errors.Wrap(err, "cannot create request")
	}

	req = req.WithContext(ctx)

	resp, err := c.Do(req) //nolint:bodyclose
	if err != nil {
		return nil, errors.Wrap(err, "cannot load products")
	}

	if resp.StatusCode != http.StatusOK {
		return nil, errors.Errorf("server returns %d: %s", resp.StatusCode, resp.Status)
	}

	return resp.Body, nil
}
