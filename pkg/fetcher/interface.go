package fetcher

import (
	"context"
	"io"
)

//go:generate mockgen -source=interface.go -destination=./mockups/fetcher.go -package=mockups

// Fetcher interface.
type Interface interface {
	// Fetch download document by URL and returns io.ReadCloser or error
	Fetch(ctx context.Context, url string) (io.ReadCloser, error)
}
